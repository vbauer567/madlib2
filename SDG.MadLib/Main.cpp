// Mad Lib
// Sawyer Grambihler

#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

string DisplayMadLib(string answers[]);
void SaveMadLib(string);

int main() {
	string answers[12];
	string prompts[12]
	{
		"Enter an adjective (describing word): ",
		"Enter a sport: ",
		"Enter a city: ",
		"Enter a person: ",
		"Enter an action verb (past tense): ",
		"Enter a vehicle: ",
		"Enter a place: ",
		"Enter a noun (thing, plural): ",
		"Enter an adjective (describing word): ",
		"Enter a food (plural): ",
		"Enter a liquid: ",
		"Enter an adjective: "
	};

	for (int i = 0; i < 12; i++) {
		cout << prompts[i];
		getline(cin, answers[i]);
	}

	string MadLib = DisplayMadLib(answers);

	cout << "\nWould you like to save your creation to a text file? Enter y for yes, or anything else for no\n";
	char print;
	cin >> print;
	print = tolower(print);

	if (print == 'y') {
		SaveMadLib(MadLib);
		cout << "Text file has been saved\n";
	}

	cout << "Press any key to exit";

	(void)_getch();
	return 0;
}

string DisplayMadLib(string answers[])
{
	string output = "One day my " + answers[0] + " friend and I decided to go to the " + answers[1] + " game in " + answers[2] + ". We really wanted to see " + answers[3] + " play. So we " + answers[4] + " in the " + answers[5] + " headed down to " + answers[6] + " and bought some " + answers[7] + ". We watched the game and it was " + answers[8] + ".We ate some " + answers[9] + " and drank some " + answers[10] + ". We had a " + answers[11] + " time, and can't wait to go again.";
	cout << "\n" << output << "\n\n";
	return output;
}

void SaveMadLib(string MadLib) {
	ofstream ofs("MadLib.txt");
	if (ofs) ofs << MadLib;
	ofs.close();
}